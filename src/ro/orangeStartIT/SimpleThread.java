package ro.orangeStartIT;

public class SimpleThread extends Thread {
    public SimpleThread(String str){
        super(str);
    }//The first method in the SimpleThread class is a constructor that takes a String as its only argument
    //His constructor is implemented by calling a superclass constructor and is interesting to us

    //Create a run() method
    public void run() {
            for (int i = 0; i < 10; i++) {
                System.out.println("Loop " + i + ": " + getName());
                //sleep(1000);
                //In each iteration the method displays the iteration number and the name of the Thread,
                //then sleeps for a random interval of up to 1 second

                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                }
            }
            System.out.println("Done." + getName());
        }
    }